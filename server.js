const fs = require('fs');
let ourData = fs.readFileSync('./data/templates.json');
let parsedData = JSON.parse(ourData);

console.log(parsedData);
const port = 3000;
var express = require('express')
var cors = require('cors')
var app = express()
 
app.use(cors());
 
app.get('/', function (req, res) {
  res.json(parsedData);
})
 
app.listen(port, function () {
  console.log(`Success! The app is listening on port ${port}`);
})