import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import App from './App.vue';
import VueCarousel from 'vue-carousel';
Vue.use(BootstrapVue, VueCarousel);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
